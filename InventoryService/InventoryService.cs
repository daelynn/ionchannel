﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;


namespace InventoryService
{
    public class InventoryService
    {
        public void GetAllMessageHandler(object model, BasicDeliverEventArgs ea)
        {
            
        }
        public ICollection<InventoryItem> GetAllInventoryItems()
        {
            return ItemRepo.GetAll();
        }

        public InventoryItem GetItemById(int id)
        {
            InventoryItem result;

            //for now, allow item not found to bubble up
            try
            {
                result = ItemRepo.GetById(id);
            }
            catch (InvalidOperationException)
            {
                throw new ItemNotFoundException();
            }

            //what if there was anotner exception type??
            return result;
        }

        public void AddNewItem(InventoryItem newItem)
        {
            ItemRepo.AddNewItem(newItem);
        }

        public void RemoveItem(InventoryItem item)
        {
            ItemRepo.RemoveItem(item);
        }
    }
}
