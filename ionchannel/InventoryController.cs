﻿using InventoryService;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace ionchannel
{
    public class InventoryController : ApiController
    {
        // GET api/inventory
        public IEnumerable<InventoryItem> Get()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "GetAll",
                                        durable: false,
                                        exclusive: false,
                                        autoDelete: false,
                                        arguments: null);

                channel.BasicPublish(exchange: "",
                                        routingKey: "GetAll",
                                        basicProperties: null);
            }

            //need to do some kind of await here until a callback occurs?
            return new InventoryService.InventoryService().GetAllInventoryItems();
        }

        // GET api/inventory/5
        public InventoryItem Get(int id)
        {
            return new InventoryItem();
        }

        // POST api/inventory
        public void Post([FromBody]InventoryItem value)
        {
        }

        // PUT api/inventory/5
        public void Put(int id, [FromBody]InventoryItem value)
        {
        }

        // DELETE api/inventory/5
        public void Delete(int id)
        {
        }
    }
}
