PROJECT SUMMARY
===============

The ionchannel solution is a web service for the handling of inventory items. The HTTP interface is handled by the .NET WebApi framework, and endpoints consume services provided by an inventory service.

PROJECT DESIGN
==============
At the interface level, this solution uses a boilerplate ApiController implementation, relying on ASP.NET's routing mechanism to route HTTP requests. The controller methods then publish messages through RabbitMQ requesting services relating to the management of InventoryItems. The InventoryService is then listening for relevant RabbitMQ messages, and handling them as they arrive.

PROJECT DECISIONS, CHALLENGES, AND RETROSPECTIVE
================================================
As with any project, decisions had to be made regarding which technologies to use. As this is a pre-interview exercise, I chose to rely, where possible, on tools I already know: Visual Studio 2015, C#, .NET, and ASP.NET. There were additional challenges present, however, in the fact that I have never before implemented any kind of service bus, nor a proper microservice. I chose RabbitMQ as the messaging queue framework, largely because it offers an easily accessible package for consumption by .NET projects. This lack of experience with microservice-driven design (MsDD?) is reflected in the current state of the project (i.e. the implementation is woefully incomplete.) Along the way, I added unit tests to ensure that regardless of what changes I made, crucial business logic remained valid and correct. While incomplete and not entirely functional, it should give a reasonable idea of my design perspective, code style, etc.

While I didn't complete the project, I do feel as though I was headed in a useful direction. I was anxious to implement the CircuitBreaker pattern, and while I didn't have time to do so in this exercise, I'll likely experiment with it on my own time.

DEPLOYMENT
==========
Deployment of this project to AWS would presumably rely on Amazon's .NET SDK and deployment tools, as described here: https://aws.amazon.com/net/ But I have no direct experience with these tools.