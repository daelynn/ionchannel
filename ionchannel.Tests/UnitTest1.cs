﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InventoryService;

namespace ionchannel.Tests
{
    [TestClass]
    public class ServiceTests
    {
        [TestMethod]
        public void GetAllReturnsAllItems()
        {
            //arrange
            var service = new InventoryService.InventoryService();

            //act
            var result = service.GetAllInventoryItems();

            //assert
            Assert.AreEqual(result.Count, 3, "GetAll returned the wrong number of items");
            //should also test that each individual item is correct.
        }

        [TestMethod]
        public void GetByIdReturnsCorrectItem()
        {
            //arrange
            var service = new InventoryService.InventoryService();

            //act
            var result = service.GetItemById(2); //magic number

            //assert
            Assert.AreEqual(result.ProductId, 2, "GetItemById returned incorrect item.");
        }

        [TestMethod]
        [ExpectedException(typeof(ItemNotFoundException))]
        public void GetByIdThrowsItemNotFoundException()
        {
            //arrange
            var service = new InventoryService.InventoryService();

            //act
            var result = service.GetItemById(5); //magic number

            //assert
            Assert.AreEqual(result.ProductId, 5, "GetItemById returned incorrect item.");

        }

        [TestMethod]
        public void AddNewItemAddsNewItem()
        {
            //arrange
            var service = new InventoryService.InventoryService();
            var newItem = new InventoryItem() { ProductId = 10 };

            //act
            service.AddNewItem(newItem);

            //assert
            Assert.IsTrue(ItemRepo.GetAll().Contains(newItem));
        }

        [TestMethod]
        public void RemoveItemRemovesItem()
        {
            //arrange
            var service = new InventoryService.InventoryService();
            var item = ItemRepo.GetById(2);

            //act
            service.RemoveItem(item);

            //assert
            Assert.IsFalse(ItemRepo.GetAll().Contains(item));

        }

        [TestMethod]
        public void RemoveItemIsSilentWhenItemDoesntExist()
        {
            //arrange
            var service = new InventoryService.InventoryService();
            var item = new InventoryItem() { ProductId = 77 };

            //act
            service.RemoveItem(item);

            //assert
            Assert.IsFalse(ItemRepo.GetAll().Contains(item));

        }
    }
}
